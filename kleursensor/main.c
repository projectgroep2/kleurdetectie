#include <msp430.h> 

#define OutputC1     BIT1
#define OutputC2     BIT2
#define OutputC3     BIT6
#define OutputC4     BIT7
#define Out          BIT0
#define Red          BIT3
#define Blue         BIT4
#define Green        BIT5
int B = 0;
int R = 0;
int G = 0;
int frequency = 0;



/*#pragma vector = TIMER0_A0_VECTOR
__interrupt void kleurdetectie(void)
{
    if (((P1IN & OutputC1) == 0) & ((P1IN & OutputC2) == 0))
    {
        R++;
        P1OUT |= Red;
        __delay_cycles(2200000);
        P1OUT &= ~(Red);
    }
    else if (((P1IN & OutputC1) == 0) & ((P1IN & OutputC2) != 0))
    {
        B++;
        P1OUT |= Blue;
        __delay_cycles(2200000);
        P1OUT &= ~(Blue);
    }
    else if (((P1IN & OutputC1) != 0) & ((P1IN & OutputC2) != 0))
    {
        G++;
        P1OUT |= Green;
        __delay_cycles(2200000);
        P1OUT &= ~(Green);
    }
    else
    {

    }
}
/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	P1DIR &= ~(OutputC2 | OutputC4 | Red | Blue | Green);
	P1DIR |= OutputC3 | Out | OutputC1;
	TACTL |= TAIE;
	//__enable_interrupt();
	/*while(1)
	{

	}*/
	if (((P1IN & OutputC1) != 0) & ((P1IN & OutputC2) != 0))
    {
        R++;
        P1DIR |= Red;
        __delay_cycles(2200000);
        P1DIR &= ~(Red);
    }
    else if (((P1IN & OutputC1) != 0) & ((P1IN & OutputC2) == 0))
    {
        B++;
        P1DIR |= Blue;
        __delay_cycles(2200000);
        P1DIR &= ~(Blue);
    }
    else if (((P1IN & OutputC1) == 0) & ((P1IN & OutputC2) == 0))
    {
        G++;
        P1DIR |= Green;
        __delay_cycles(2200000);
        P1DIR &= ~(Green);
    }
    else
    {

    }
	return 0;
}
