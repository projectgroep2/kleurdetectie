#include <msp430.h> 

#define S0           BIT1
#define S1           BIT2
#define S2           BIT6
#define S3           BIT7
#define sensorOut    BIT0
#define Red          BIT3
#define Blue         BIT4
#define Green        BIT5
int B = 0;
int R = 0;
int G = 0;
int frequency = 0;
unsigned short Data[20];
unsigned short Red,Green,Blue,Brightness;
unsigned short Count = 0;
unsigned char over = 0;
/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	P1DIR |= S0 | S2 | S3;
	P1DIR &= ~(S1 | Red | Green | Blue);
	P1OUT |= S0;
	P1OUT &= ~(S1);

	while (1)
	{
	    switch(frequency)
	    {
	    case Red:
	        P1OUT &= ~(S2 | S3);
	        R++;
	        P1OUT |= Red;
	        __delay_cycles(2200000);
	        P1OUT &= ~(Red);
	    break;
	    case Blue:
	        P1OUT &= ~(S2);
	        P1OUT |= S3;
            B++;
            P1OUT |= Blue;
            __delay_cycles(2200000);
            P1OUT &= ~(Blue);
        break;
	    case Green:
	        P1DIR |= S2 | S3;
            G++;
            P1DIR |= Green;
            __delay_cycles(2200000);
            P1DIR &= ~(Green);
	    }
	}
	
	return 0;
}
