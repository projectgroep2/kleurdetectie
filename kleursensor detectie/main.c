#include <msp430.h> 
/*** Global variables ***/
volatile unsigned int CounterValue = 0;
volatile unsigned int StoredCount = 0;
unsigned int Result = 0;
unsigned int ResultG = 0;
unsigned int ResultB = 0;
int B = 0;
int R = 0;
int G = 0;
#define S0           BIT3
#define S1           BIT4
#define S2           BIT1
#define S3           BIT2
#define sensorOut    BIT0
#define lBlue         BIT3
#define lGreen        BIT4
#define lRed       BIT5


void kleur(int kleur)
{
if(kleur == 1)
{
    P1OUT &= ~(S2 | S3);

    TA0CTL |= MC0;              // Start timer
    TA1CTL |= MC1;              // Start timer
    while(CounterValue != 30); // Wait while CounterValue is not equal to 400
    TA0CTL &= ~MC0;             // Stop timer
    TA1CTL &= ~MC1;             // Stop timer
    Result = StoredCount;       // Store frequency in Result
    CounterValue = 0;           // Set CounterValue to 0
    StoredCount = 0;            // Set StoredCount to 0
    TA0R = 0;                   // Set Timer_A0 register to 0
    TA1R = 0;                   // Set Timer_A1 register to 0

    //RED
    if((Result >= 2100 && Result <= 2300) &&
      (ResultB >= 900 && ResultB <= 1200) &&
      (ResultG >= 600 && ResultG <= 900))
        {
           P2OUT &= ~(lBlue | lGreen);
           P2OUT |= lRed;
           R++;
           __delay_cycles(15000000);
           P2OUT &= ~(lRed);
        }
}
if(kleur == 2)
{
    P1OUT |= (S2 | S3);


    TA0CTL |= MC0;              // Start timer
    TA1CTL |= MC1;              // Start timer
    while(CounterValue != 30); // Wait while CounterValue is not equal to 400
    TA0CTL &= ~MC0;             // Stop timer
    TA1CTL &= ~MC1;             // Stop timer
    ResultG = StoredCount;       // Store frequency in Result
    CounterValue = 0;           // Zero CounterValue
    StoredCount = 0;            // Zero StoredCount
    TA0R = 0;                   // Zero Timer_A0 register
    TA1R = 0;                   // Zero Timer_A1 register

    //GREEN
    if ((ResultG >= 1100 && ResultG <= 1400) &&
        (Result >= 800 && Result <= 1100) &&
        (ResultB >= 900 && ResultB <= 1200))
        {
            P2OUT &= ~(lRed | lBlue);
            P2OUT |= lGreen;
            G++;
            __delay_cycles(15000000);
            P2OUT &= ~(lGreen);
        }
}
if(kleur == 3)
{
    P1OUT &= ~(S2);
    P1OUT |= (S3);

    TA0CTL |= MC0;              // Start timer
    TA1CTL |= MC1;              // Start timer

    while(CounterValue != 30); // Wait while CounterValue is not equal to 400
    TA0CTL &= ~MC0;             // Stop timer
    TA1CTL &= ~MC1;             // Stop timer
    ResultB = StoredCount;      // Store frequency in Result
    CounterValue = 0;           // Zero CounterValue
    StoredCount = 0;            // Zero StoredCount
    TA0R = 0;                   // Zero Timer_A0 register
    TA1R = 0;                   // Zero Timer_A1 register


    // BLUE
    if ((ResultB >= 1500 && ResultB <= 1800) &&
      (Result >= 500 && Result <= 800) &&
      (ResultG >= 600 && ResultG <= 900))
        {
           P2OUT &= ~(lRed | lGreen);
           P2OUT |= lBlue;
           B++;
           __delay_cycles(15000000);
           P2OUT &= ~(lBlue);
        }


}
}

int main(void) {

    /*** Watchdog timer and clock Set-Up ***/
    WDTCTL = WDTPW + WDTHOLD;       // Stop watchdog timer
    BCSCTL1 = CALBC1_8MHZ;          // Set range
    DCOCTL = CALDCO_8MHZ;           // Set DCO step + modulation

    /*** GPIO Set-Up ***/
    P2DIR &= sensorOut;             // P2.0 als input
    P2SEL |= sensorOut;              // P2.0 set to primary peripheral Timer1_A

    P1DIR |= (S0 | S1 | S2 | S3);
    P1OUT &= ~(S2 | S3);
    P1OUT |= (S0 | S1);

    P2DIR |= lRed | lBlue | lGreen;
    P2OUT &= ~(lRed | lBlue | lGreen);

    /*** Timer_A Set-Up ***/
    TA0CCR0 = 20000;            // 20000*400 = 8000000 or 8MHz
    TA0CCTL0 |= CCIE;           // Interrupt enable
    TA0CTL |= TASSEL_2;         // SMCLK

    TA1CCTL0 |= CCIE + CCIS_0 +

    CM_2 + CAP;                 // Interrupt enable, capture select CCIxA, Positive edge, capture mode
    TA1CTL |= TASSEL_2;                     // SMCLK

    _BIS_SR(GIE);               // Enter LPM0 with interrupts enabled





    while(1)
    {


    kleur(1);
    kleur(2);
    kleur(3);

}
}
    //Timer_A0 TACCR0 Interrupt Vector Handler Routine
    #pragma vector=TIMER0_A0_VECTOR
    __interrupt void Timer0_A0(void)
    {
        CounterValue++;


    }

    //Timer_A1 TACCR0 Interrupt Vector Handler Routine
    #pragma vector=TIMER1_A0_VECTOR
    __interrupt void Timer1_A0(void)
    {
        StoredCount++;

    }
